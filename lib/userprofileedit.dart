import 'package:flutter/material.dart';

class ProfileEditScreen extends StatelessWidget {
  const ProfileEditScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PikPoket',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Profile Edit Page'),
        ),
        body: const Center(
          child: Text('Edit your profile on this page'),
        ),

      ),
    );
  }
}