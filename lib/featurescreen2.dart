import 'package:flutter/material.dart';

class FeatureScreen2 extends StatelessWidget {
  const FeatureScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PikPoket',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('FeatureScreen2'),
        ),
        body: const Center(
          child: Text('FeatureScreen2 will appear on this page'),
        ),
    floatingActionButton: FloatingActionButton(
        tooltip: 'Do to Next Page',
        child: const Icon(Icons.navigate_next),
        onPressed: (){
            }
          ),
        ) );
      }
    }