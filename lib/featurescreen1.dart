import 'package:flutter/material.dart';

class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}

@override

Widget build(BuildContext context) {
  return MaterialApp(
    title: 'PikPoket',
    home: Scaffold(
      appBar: AppBar(
        title: const Text('FeatureScreen1'),
      ),
      body: const Center(
        child: Text('FeatureScreen1 features will appear on this page'),
      ),
  floatingActionButton: FloatingActionButton(
          tooltip: 'Do to Next Page',
          child: const Icon(Icons.navigate_next),
          onPressed: (){
          Navigator.push(
          context,
          MaterialPageRoute(
          builder: (context) => const FeatureScreen1())
            );
          }
        )));
      }


